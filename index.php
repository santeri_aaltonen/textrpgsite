<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
<title>Gamesitelol</title>
<link rel="stylesheet" href="styles.css">
</head>

<body>
<div class=wrapper>
<?php
require 'header.php';

// Set the default name
$action = 'home';
// Specify some disallowed paths
$disallowed_paths = array('header', 'footer');
if(isset($_GET['page']) && $_GET['page']!=""){
	$page = "";
	switch ($_GET['page']) {
		case 'home':
			$page = "home.php";
			break;

		case 'news':
			$page = "news.php";
			break;

		case 'games':
			$page = "games.php";
			break;

		case 'contact':
			$page = "contact.php";
			break;
			
		case 'about':
			$page = "about.php";
			break;
			
		case 'game1':
		$page = "game1.php";
		break;

		default:
			$page = "home.php";
			break;
            }

            include($page);
        }
require 'footer.php';
?>
</div>
</body> 
</html>